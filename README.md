# Localyze Project

Hello everyone!

I ended up running only challenge #3 but my intention was to create a “semi-complete” API project so you would have more content to evaluate.

I'll explain below what each layer of the project aims to:

**1 - Model**
Well, like every model, it is the layer of direct interaction with the data source and it is where the business rules are applied, that is, most of the challenge rules are being validated by it.

**2 - Service**
The Service layer comes before the Model and it is responsible for also validating and mainly preparing the data that will be sent to the Model and Presenter.

**3 - Presenter**
The Presenter is responsible for shaping the format of the responses that will be passed to the Controller.

**4 - Controller**
The Controller is the input and output port of data. In it, we receive the request that is handled if necessary and forwarded to the Service. After the transfer, the Controller receives the data processed by the Presenter to finally expose this data to the requester.

The Project was created using the Jets framework. To learn more about it, go to [Ruby on Jets website](http://rubyonjets.com).

To download and run the Project on your local machine:

**Prerequisites:**
- MySQL 5 running locally
- Ruby version 2.7.x
- Jets version 3.0.x


**Installation of Jets:**
The installation of Jets is very simple, just run 
- gem install jets

**clone the project**
- git clone https://gitlab.com/emaildoleandromaciel/localyze.git .

**Browse to the project folder**
- cd localyze

**Install dependencies**

- bundle install

_If your local MySQL is set to localhost and user and password are root, you are good to go, otherwise I've included the .env.test file to simplify and you can change your database credentials in there. After credentials where all set run the following command:_

- JETS_ENV=test jets db:create db:migrate

**Now you can run the tests to see if everything is OK**

- bundle exec rspec

**Testing the API**
only two methods were created, POST and GET and to test them, run Jets locally by running the command:

- JETS_ENV=test jets server

A server will be started on your port 8888. After that, just make the calls informed below using Postman or any other tool of your choice to create the 3 last word counter:

- ENDPOINT --> localhost:8888/char-counters
- METHOD --> POST
- PAYLOAD FORMAT --> JSON
```
{
    "first_string": "any string",
    "second_string": "another one",
    "third_string": "this is the last one" 
}
```
- ENDPOINT --> localhost:8888/char-counters/:char_counter_id
- METHOD --> GET

I hope it is enough and I apologize if the challenge was not to your liking.

Any questions, just let me know.





