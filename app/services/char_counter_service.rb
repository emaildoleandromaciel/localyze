class CharCounterService < ApplicationService
  def find_by_uuid
    @model = CharCounter.find_by_uuid(@params[:uuid])
    return [@model, :counter_not_found] if @model.nil?

    [@model, :ok]
  end

  def check_string
    first_string = @params['first_string'].split.last
    second_string = @params['second_string'].split.last
    third_string = @params['third_string'].split.last

    return false if first_string.nil?
    return false if second_string.nil?
    return false if third_string.nil?

    true
  end

  def check_last_word
    return false if @params['first_string'].split.last.delete(' ').size.zero?
    return false if @params['second_string'].split.last.delete(' ').size.zero?
    return false if @params['third_string'].split.last.delete(' ').size.zero?

    true
  end

  def update_model_params
    @model.first_last_word_counter = @params['first_string'].split.last.delete(' ').size
    @model.second_last_word_counter = @params['second_string'].split.last.delete(' ').size
    @model.third_last_word_counter = @params['third_string'].split.last.delete(' ').size
  end

  def create
    @model = CharCounter.new(@params)

    return [@model, :validation_error] unless @model.valid?

    return [@model, :no_string_value_detected] unless check_string
    return [@model, :no_last_word_detected] unless check_last_word

    update_model_params

    return [@model, :validation_error] unless @model.valid?

    @model.save

    [@model, :created]
  end

end