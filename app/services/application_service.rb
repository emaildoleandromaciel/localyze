class ApplicationService

  def initialize(model: nil, params: nil)
    @model = model
    @params = params
  end

end
