class CharCounterPresenter < ApplicationPresenter
  URL = '/char-counters/:char_counter_id'

  HYPERMIDIA = [
    [
      'href' => URL,
      'rel' => 'self',
      'type' => 'GET'
    ]
  ]

  def success(object = nil)
    obj  = object.nil? ? @object : object
    uuid = obj.uuid
    json_data = obj.attributes.except('uuid')
    json_data['id'] = uuid
    json_data['links'] = get_links(obj)
    json_data
  end

  def get_links(obj)
    HYPERMIDIA.map { |action|
      {
        'href' => action[0]['href'].gsub(':char_counter_id', obj.uuid),
        'rel' => action[0]['rel'],
        'type' => action[0]['type']
      }
    }
  end

end
