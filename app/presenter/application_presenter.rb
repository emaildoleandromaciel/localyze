require 'active_support/core_ext'

class ApplicationPresenter

  def initialize(object = nil, code = nil, params = nil)
    @object = object
    @code   = code
    @params = params
  end

  def as_json
    body = valid? ? success : error
    { json: body, status: get_http_status }
  end

  def as_json_collection(pagination_info = nil)

    headers = {
      "X-Total-Items" => pagination_info[:total_items].to_s,
      "X-Total-Pages" => pagination_info[:total_pages].to_s,
      "Link"          => build_link_http_header(pagination_info[:page],
                                                pagination_info[:per_page],
                                                pagination_info[:total_pages])
    } unless pagination_info.nil?

    body = @object.map { |m| success(m) }
    rendered_response = { json: body, status: get_http_status }

    return [rendered_response, headers] unless pagination_info.nil?

    rendered_response
  end

  # Presenter must consider valid objects that:
  # 1. Are not nil
  # 2. Valid Model (no validation errors)
  # 3. Http Status Code beeing of the 2xx family
  def valid?
    !@object.nil? and @object.valid? and (get_http_status < 300)
  end

  # who extends this class must implements 'success' method
  def success(object = nil)
    {
      'code': 'not_implemented'
    }
  end

  def error
    error           = {}
    error[:status]  = get_http_status
    error[:code]    = get_error_code
    error[:message] = get_error_message
    error[:errors]  = @object.errors.messages if !(@object.nil? or @object.errors.blank?)
    error
  end

  def get_http_status
    HTTP_STATUSES[@code]
  end

  def get_error_code
     "#{ERROR_CODES[@code]}_error"
  end

  def get_error_message
    ERROR_MESSAGES[@code]
  end

  def build_link_http_header(page, per_page, total_pages)
    link_http_header  = build_link_item_http_header(page, per_page, total_pages, 'first')
    link_http_header += build_link_item_http_header(page, per_page, total_pages, 'prev')
    link_http_header += build_link_item_http_header(page, per_page, total_pages, 'next')
    link_http_header += build_link_item_http_header(page, per_page, total_pages, 'last')
    link_http_header
  end

  def build_link_item_http_header(page, per_page, total_pages, rel)

    base_url      = ENV['BASE_URL']
    finalization  = ', '
    pages = page.to_i

    if (rel == 'first')
      pages = 1
    elsif (rel == 'prev')
      if(pages > 1)
        pages -= 1
      else
        return ""
      end
    elsif (rel == 'next')
      if(pages < total_pages)
        pages += 1
      else
        return ""
      end
    elsif (rel == 'last')
      page = total_pages
      finalization = ''
    end

    link_item = "<#{base_url}/#{get_find_all_resource}&page=#{pages}&per_page=#{per_page}>; rel='#{rel}'#{finalization}"
    link_item
  end

  # who extends this class must implements 'get_find_all_resource' method
  def get_find_all_resource
    'not implemented'
  end

  HTTP_STATUSES = {
    :validation_error              => 422,
    :created                       => 201,
    :loaded                        => 200,
    :ok                            => 200,
    :no_content                    => 204,
    :item_not_found                => 404,
    :counter_not_found             => 404,
    :transition_not_accepted       => 422,
    :idempotency_key_not_present   => 422,
    :internal_server_error         => 500,
    :bad_gateway                   => 502,
    :unauthorized                  => 401,
    :not_found                     => 404
  }

  ERROR_CODES = {
    :validation_error               => :validation,
    :not_found                      => :item_not_found,
    :idempotency_key_not_present    => :idempotency_key,
    :transition_not_accepted        => :transition_not_accepted,
    :no_string_value_detected       => :validation_error,
    :no_last_word_detected          => :validation_error,
    :internal_server_error          => :internal_server,
    :bad_gateway                    => :bad_gateway,
    :counter_not_found              => :item_not_found,
    :unauthorized_client_id         => :unauthorized
  }

  ERROR_MESSAGES = {
    :validation_error               => 'One or more fields are required and / or in an invalid format',
    :no_string_value_detected       => 'One or more fields has no value',
    :no_last_word_detected          => 'One or more fields has no last word',
    :item_not_found                 => 'The requested item was not found',
    :transition_not_accepted        => 'The requested transition of status is not accepted',
    :idempotency_key_not_present    => 'The custom header field X-Idempotency-Key is required',
    :internal_server_error          => 'Internal Server Error',
    :kind_not_available             => 'Kind Not Available',
    :counter_not_found              => 'Char Counter not found',
    :unauthorized_client_id         => 'Unauthorized client_id'
  }
end
