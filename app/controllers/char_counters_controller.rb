class CharCountersController < ApplicationController
  before_action :set_model, only: %i[show]

  # GET /char-counters/1
  def show
    render_data(@model, @code)
  end

  # POST /char-counters
  def create
    service = CharCounterService.new(params: set_params)
    model, code = service.create
    render_data(model, code)
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_model
    service = CharCounterService.new(params: { uuid: params['char_counter_id'] })
    @model, @code = service.find_by_uuid
  end

  def render_data(model, code)
    presenter = CharCounterPresenter.new(model, code)
    render presenter.as_json
  end

  # Only allow a trusted parameter "white list" through.
  def set_params
    params.permit(
      :first_string,
      :second_string,
      :third_string
    )
  end
end
