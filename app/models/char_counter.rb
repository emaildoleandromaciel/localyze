class CharCounter < ApplicationRecord

  MIN_CHAR_COUNT = 1
  MAX_CHAR_COUNT = 104
  VALID_STRING_REGEX = /\A[a-z\s]*$\z/ismu

  validates :first_string, :second_string, :third_string, length: { minimum: MIN_CHAR_COUNT, maximum: MAX_CHAR_COUNT }, presence: true, format: { with: VALID_STRING_REGEX }
  validates :first_last_word_counter, :second_last_word_counter, :third_last_word_counter, allow_blank: true, numericality: { only_integer: true, greater_than: 0 }

  before_create do
    self.uuid = UUID.new.generate
  end

end
