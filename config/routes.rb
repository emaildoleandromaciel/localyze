Jets.application.routes.draw do
  # Char Counters resources
  get '/char-counters/:char_counter_id', to: 'char_counters#show'
  post '/char-counters', to: 'char_counters#create'
end
