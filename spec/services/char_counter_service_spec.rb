describe CharCounterService do
  before(:each) do
    @char_counter_payload = payload('valid-char-counter').except('first_last_word_counter', 'second_last_word_counter', 'third_last_word_counter')
  end

  after(:all) do
    CharCounter.all.destroy_all
  end

  it 'is not valid without a first_string' do
    @char_counter_payload['first_string'] = nil
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create
    expect(code).to eq(:validation_error)
    expect(model.id).to be_nil
  end

  it 'is not valid without a second_string' do
    @char_counter_payload['second_string'] = nil
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create
    expect(code).to eq(:validation_error)
    expect(model.id).to be_nil
  end

  it 'is not valid without a third_string' do
    @char_counter_payload['third_string'] = nil
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create
    expect(code).to eq(:validation_error)
    expect(model.id).to be_nil
  end

  it 'is not valid without a first_last_word_count' do
    @char_counter_payload['first_string'] = ' '
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create
    expect(code).to eq(:validation_error)
    expect(model.id).to be_nil
  end

  it 'is not valid without a second_last_word_count' do
    @char_counter_payload['second_string'] = ' '
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create
    expect(code).to eq(:validation_error)
    expect(model.id).to be_nil
  end

  it 'is not valid without a third_last_word_count' do
    @char_counter_payload['third_string'] = ' '
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create
    expect(code).to eq(:validation_error)
    expect(model.id).to be_nil
  end

  it 'is not valid if first_string is bigger than 104 chars' do
    @char_counter_payload['first_string'] = 'sfsdfsasdfasdf asdfsafasdfasfd asdfasdfasdfasdfasdf asdfasdfasdfasdfasdfasdfasdf asdfasdfasdfasdfasdfasdfas asdfasdfasdfasdfasdfas'
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create
    expect(code).to eq(:validation_error)
    expect(model.id).to be_nil
  end

  it 'is not valid if second_string is bigger than 104 chars' do
    @char_counter_payload['second_string'] = 'sfsdfsasdfasdf asdfsafasdfasfd asdfasdfasdfasdfasdf asdfasdfasdfasdfasdfasdfasdf asdfasdfasdfasdfasdfasdfas asdfasdfasdfasdfasdfas'
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create
    expect(code).to eq(:validation_error)
    expect(model.id).to be_nil
  end

  it 'is not valid if third_string is bigger than 104 chars' do
    @char_counter_payload['third_string'] = 'sfsdfsasdfasdf asdfsafasdfasfd asdfasdfasdfasdfasdf asdfasdfasdfasdfasdfasdfasdf asdfasdfasdfasdfasdfasdfas asdfasdfasdfasdfasdfas'
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create
    expect(code).to eq(:validation_error)
    expect(model.id).to be_nil
  end

  it 'is not valid if first_string has no English chars' do
    @char_counter_payload['first_string'] = 'asdasdfçç ásaçéúdfsdf'
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create
    expect(code).to eq(:validation_error)
    expect(model.id).to be_nil
  end

  it 'is not valid if second_string has no English chars' do
    @char_counter_payload['second_string'] = 'asdasdfçç ásaçéúdfsdf'
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create
    expect(code).to eq(:validation_error)
    expect(model.id).to be_nil
  end

  it 'is not valid if third_string has no English chars' do
    @char_counter_payload['third_string'] = 'asdasdfçç ásaçéúdfsdf'
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create
    expect(code).to eq(:validation_error)
    expect(model.id).to be_nil
  end

  it 'is not valid if first_string has numeric chars' do
    @char_counter_payload['first_string'] = 'asd123asdf sadfsdf'
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create
    expect(code).to eq(:validation_error)
    expect(model.id).to be_nil
  end

  it 'is not valid if second_string has numeric chars' do
    @char_counter_payload['second_string'] = 'asd123asdf sadfsdf'
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create
    expect(code).to eq(:validation_error)
    expect(model.id).to be_nil
  end

  it 'is not valid if third_string has numeric chars' do
    @char_counter_payload['third_string'] = 'asd123asdf sadfsdf'
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create
    expect(code).to eq(:validation_error)
    expect(model.id).to be_nil
  end

  it 'must have a first_last_word_counter bigger than 0 after create' do
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create
    expect(code).to eq(:created)
    expect(model.id).to_not be_nil
    expect(model.first_last_word_counter).to be >= 1
  end

  it 'must have a second_last_word_counter bigger than 0 after create' do
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create
    expect(code).to eq(:created)
    expect(model.id).to_not be_nil
    expect(model.second_last_word_counter).to be >= 1
  end

  it 'must have a third_last_word_counter bigger than 0 after create' do
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create
    expect(code).to eq(:created)
    expect(model.id).to_not be_nil
    expect(model.third_last_word_counter).to be >= 1
  end

  it 'must have a id after create' do
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create
    expect(code).to eq(:created)
    expect(model.id).to_not be_nil
  end

  it 'must have a uuid after create' do
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create
    expect(code).to eq(:created)
    expect(model.uuid).to_not be_nil
  end

  it 'must find by uuid' do
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create
    expect(code).to eq(:created)
    expect(model.uuid).to_not be_nil

    service = CharCounterService.new(params: { uuid: model.uuid })
    model, code = service.find_by_uuid

    expect(code).to eq(:ok)
    expect(model.uuid).to_not be_nil
  end

  it 'must return :counter_not_found when not found' do
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create
    expect(code).to eq(:created)
    expect(model.uuid).to_not be_nil

    service = CharCounterService.new(params: { uuid: 'sdad' })
    model, code = service.find_by_uuid

    expect(code).to eq(:counter_not_found)
    expect(model).to be_nil
  end

end
