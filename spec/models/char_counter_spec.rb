describe CharCounter, type: :model do
  before(:each) do
    char_counter_payload = payload('valid-char-counter')
    @valid_char_counter = CharCounter.new(char_counter_payload)
  end

  after(:all) do
    CharCounter.all.destroy_all
  end

  it 'is not valid without a first_string' do
    @valid_char_counter.first_string = nil
    expect(@valid_char_counter).to_not be_valid
  end

  it 'is not valid without a second_string' do
    @valid_char_counter.second_string = nil
    expect(@valid_char_counter).to_not be_valid
  end

  it 'is not valid without a third_string' do
    @valid_char_counter.third_string = nil
    expect(@valid_char_counter).to_not be_valid
  end

  it 'is not valid if first_last_word_counter is not a integer bigger than 0' do
    @valid_char_counter.first_last_word_counter = 0
    expect(@valid_char_counter).to_not be_valid
  end

  it 'is not valid if second_last_word_counter is not a integer bigger than 0' do
    @valid_char_counter.second_last_word_counter = 0
    expect(@valid_char_counter).to_not be_valid
  end

  it 'is not valid if third_last_word_counter is not a integer bigger than 0' do
    @valid_char_counter.third_last_word_counter = 0
    expect(@valid_char_counter).to_not be_valid
  end

  it 'is not valid if first_string has non English chars' do
    @valid_char_counter.first_string = 0
    expect(@valid_char_counter).to_not be_valid
  end

  it 'is not valid if second_string has non English chars' do
    @valid_char_counter.second_string = 0
    expect(@valid_char_counter).to_not be_valid
  end

  it 'is not valid if third_string has non English chars' do
    @valid_char_counter.third_string = 0
    expect(@valid_char_counter).to_not be_valid
  end

  it 'is not valid if first_string has less than 1 char' do
    @valid_char_counter.first_string = ' '
    expect(@valid_char_counter).to_not be_valid
  end

  it 'is not valid if second_string has less than 1 char' do
    @valid_char_counter.second_string = ' '
    expect(@valid_char_counter).to_not be_valid
  end

  it 'is not valid if third_string has less than 1 char' do
    @valid_char_counter.third_string = ' '
    expect(@valid_char_counter).to_not be_valid
  end

  it 'is not valid if first_string has more than 104 chars' do
    @valid_char_counter.first_string = ' sadfsdfj kasjdhfkjsf kjsdhfkjashdfkjsahdfkjsaf kajsdhfksjhfksjhdfkjsdhfksjdfhkjsdf kasdjfhksjdhfksjadhfksj '
    expect(@valid_char_counter).to_not be_valid
  end

  it 'is not valid if second_string has more than 104 chars' do
    @valid_char_counter.second_string = ' sadfsdfj kasjdhfkjsf kjsdhfkjashdfkjsahdfkjsaf kajsdhfksjhfksjhdfkjsdhfksjdfhkjsdf kasdjfhksjdhfksjadhfksj '
    expect(@valid_char_counter).to_not be_valid
  end

  it 'is not valid if third_string has more than 104 chars' do
    @valid_char_counter.third_string = ' sadfsdfj kasjdhfkjsf kjsdhfkjashdfkjsahdfkjsaf kajsdhfksjhfksjhdfkjsdhfksjdfhkjsdf kasdjfhksjdhfksjadhfksj '
    expect(@valid_char_counter).to_not be_valid
  end

  it 'generate an UUID when create a new Account' do
    @valid_char_counter.save
    expect(@valid_char_counter.uuid).to_not be_nil
  end

end
