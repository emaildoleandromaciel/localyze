describe CharCounterPresenter do
  before(:each) do
    @char_counter_payload = payload('valid-char-counter').except('first_last_word_counter', 'second_last_word_counter', 'third_last_word_counter')
  end

  after(:all) do
    CharCounter.all.destroy_all
  end

  it 'must return Http Status Code 422 if it is without a first_string' do
    @char_counter_payload['first_string'] = nil
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create

    presenter = CharCounterPresenter.new(model, code)
    response = presenter.as_json
    expect(response[:status]).to eq(422)
  end

  it 'must return Http Status Code 422 if it is without a second_string' do
    @char_counter_payload['second_string'] = nil
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create

    presenter = CharCounterPresenter.new(model, code)
    response = presenter.as_json
    expect(response[:status]).to eq(422)
  end

  it 'must return Http Status Code 422 if it is without a third_string' do
    @char_counter_payload['third_string'] = nil
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create

    presenter = CharCounterPresenter.new(model, code)
    response = presenter.as_json
    expect(response[:status]).to eq(422)
  end

  it 'must return Http Status Code 422 if first_string is a empty string' do
    @char_counter_payload['first_string'] = ' '
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create

    presenter = CharCounterPresenter.new(model, code)
    response = presenter.as_json
    expect(response[:status]).to eq(422)
  end

  it 'must return Http Status Code 422 if second_string is a empty string' do
    @char_counter_payload['second_string'] = ' '
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create

    presenter = CharCounterPresenter.new(model, code)
    response = presenter.as_json
    expect(response[:status]).to eq(422)
  end

  it 'must return Http Status Code 422 if third_string is a empty string' do
    @char_counter_payload['third_string'] = ' '
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create

    presenter = CharCounterPresenter.new(model, code)
    response = presenter.as_json
    expect(response[:status]).to eq(422)
  end

  it 'must return Http Status Code 422 if first_string is bigger than 104 chars' do
    @char_counter_payload['first_string'] = 'sfsdfsasdfasdf asdfsafasdfasfd asdfasdfasdfasdfasdf asdfasdfasdfasdfasdfasdfasdf asdfasdfasdfasdfasdfasdfas asdfasdfasdfasdfasdfas'
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create

    presenter = CharCounterPresenter.new(model, code)
    response = presenter.as_json
    expect(response[:status]).to eq(422)
  end

  it 'must return Http Status Code 422 if second_string is bigger than 104 chars' do
    @char_counter_payload['second_string'] = 'sfsdfsasdfasdf asdfsafasdfasfd asdfasdfasdfasdfasdf asdfasdfasdfasdfasdfasdfasdf asdfasdfasdfasdfasdfasdfas asdfasdfasdfasdfasdfas'
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create

    presenter = CharCounterPresenter.new(model, code)
    response = presenter.as_json
    expect(response[:status]).to eq(422)
  end

  it 'must return Http Status Code 422 if third_string is bigger than 104 chars' do
    @char_counter_payload['third_string'] = 'sfsdfsasdfasdf asdfsafasdfasfd asdfasdfasdfasdfasdf asdfasdfasdfasdfasdfasdfasdf asdfasdfasdfasdfasdfasdfas asdfasdfasdfasdfasdfas'
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create

    presenter = CharCounterPresenter.new(model, code)
    response = presenter.as_json
    expect(response[:status]).to eq(422)
  end

  it 'must return Http Status Code 422 if first_string has no English chars' do
    @char_counter_payload['first_string'] = 'asdasdfçç ásaçéúdfsdf'
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create

    presenter = CharCounterPresenter.new(model, code)
    response = presenter.as_json
    expect(response[:status]).to eq(422)
  end

  it 'must return Http Status Code 422 if second_string has no English chars' do
    @char_counter_payload['second_string'] = 'asdasdfçç ásaçéúdfsdf'
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create

    presenter = CharCounterPresenter.new(model, code)
    response = presenter.as_json
    expect(response[:status]).to eq(422)
  end

  it 'must return Http Status Code 422 if third_string has no English chars' do
    @char_counter_payload['third_string'] = 'asdasdfçç ásaçéúdfsdf'
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create

    presenter = CharCounterPresenter.new(model, code)
    response = presenter.as_json
    expect(response[:status]).to eq(422)
  end

  it 'must return Http Status Code 422 if first_string has numeric chars' do
    @char_counter_payload['first_string'] = 'asd123asdf sadfsdf'
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create

    presenter = CharCounterPresenter.new(model, code)
    response = presenter.as_json
    expect(response[:status]).to eq(422)
  end

  it 'must return Http Status Code 422 if second_string has numeric chars' do
    @char_counter_payload['second_string'] = 'asd123asdf sadfsdf'
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create

    presenter = CharCounterPresenter.new(model, code)
    response = presenter.as_json
    expect(response[:status]).to eq(422)
  end

  it 'must return Http Status Code 422 if third_string has numeric chars' do
    @char_counter_payload['third_string'] = 'asd123asdf sadfsdf'
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create

    presenter = CharCounterPresenter.new(model, code)
    response = presenter.as_json
    expect(response[:status]).to eq(422)
  end

  it 'must return Http Status Code 201 if first_last_word_counter is bigger than 0 after create' do
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create

    presenter = CharCounterPresenter.new(model, code)
    response = presenter.as_json
    expect(response[:status]).to eq(201)
  end

  it 'must return Http Status Code 201 if second_last_word_counter is bigger than 0 after create' do
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create

    presenter = CharCounterPresenter.new(model, code)
    response = presenter.as_json
    expect(response[:status]).to eq(201)
  end

  it 'must return Http Status Code 201 if third_last_word_counter is bigger than 0 after create' do
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create

    presenter = CharCounterPresenter.new(model, code)
    response = presenter.as_json
    expect(response[:status]).to eq(201)
  end

  it 'must present an id after create' do
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create

    presenter = CharCounterPresenter.new(model, code)
    response = presenter.as_json
    expect(response[:status]).to eq(201)
    expect(response[:json]['id']).to_not be_nil
  end

  it 'must not present an uuid after create' do
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create

    presenter = CharCounterPresenter.new(model, code)
    response = presenter.as_json
    expect(response[:status]).to eq(201)
    expect(response[:json]['uuid']).to be_nil
  end

  it 'must return Http Status Code 200 when found by uuid' do
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create
    expect(code).to eq(:created)
    expect(model.uuid).to_not be_nil

    service = CharCounterService.new(params: { uuid: model.uuid })
    model, code = service.find_by_uuid

    presenter = CharCounterPresenter.new(model, code)
    response = presenter.as_json
    expect(response[:status]).to eq(200)
  end

  it 'must return Http Status Code 404 when not found' do
    service = CharCounterService.new(params: @char_counter_payload)
    model, code = service.create
    expect(code).to eq(:created)
    expect(model.uuid).to_not be_nil

    service = CharCounterService.new(params: { uuid: 'sdad' })
    model, code = service.find_by_uuid

    presenter = CharCounterPresenter.new(model, code)
    response = presenter.as_json
    expect(response[:status]).to eq(404)
  end

end
