describe CharCountersController, type: :controller do
  before(:each) do
    @char_counter_payload = payload('valid-char-counter').except('first_last_word_counter', 'second_last_word_counter', 'third_last_word_counter')
  end

  after(:all) do
    CharCounter.all.destroy_all
  end

  it 'returns 422 if first_string is null' do
    @char_counter_payload['first_string'] = nil
    post '/char-counters', params: @char_counter_payload
    response_data = JSON.parse(response.body)
    expect(response.status).to eq(422)
    expect(response_data['id']).to be_nil
  end

  it 'returns 422 if second_string is null' do
    @char_counter_payload['second_string'] = nil
    post '/char-counters', params: @char_counter_payload
    response_data = JSON.parse(response.body)
    expect(response.status).to eq(422)
    expect(response_data['id']).to be_nil
  end

  it 'returns 422 if third_string is null' do
    @char_counter_payload['third_string'] = nil
    post '/char-counters', params: @char_counter_payload
    response_data = JSON.parse(response.body)
    expect(response.status).to eq(422)
    expect(response_data['id']).to be_nil
  end

  it 'returns 422 if first_string is a empty string' do
    @char_counter_payload['first_string'] = ' '
    post '/char-counters', params: @char_counter_payload
    response_data = JSON.parse(response.body)
    expect(response.status).to eq(422)
    expect(response_data['id']).to be_nil
  end

  it 'returns 422 if second_string is a empty string' do
    @char_counter_payload['second_string'] = ' '
    post '/char-counters', params: @char_counter_payload
    response_data = JSON.parse(response.body)
    expect(response.status).to eq(422)
    expect(response_data['id']).to be_nil
  end

  it 'returns 422 if third_string is a empty string' do
    @char_counter_payload['third_string'] = ' '
    post '/char-counters', params: @char_counter_payload
    response_data = JSON.parse(response.body)
    expect(response.status).to eq(422)
    expect(response_data['id']).to be_nil
  end

  it 'returns 422 if first_string has more than 104 chars' do
    @char_counter_payload['first_string'] = 'sdfsafsa sadfsadfsadfsaf asdfsadfasdfasdf asdfasdfasdfasdf asdfasfsadfsdf asdfasdfsdfsadf asdfsfsdfsf sadfsadfsadfsafsdaf sadfasfsafasdfsd '
    post '/char-counters', params: @char_counter_payload
    response_data = JSON.parse(response.body)
    expect(response.status).to eq(422)
    expect(response_data['id']).to be_nil
  end

  it 'returns 422 if second_string has more than 104 chars' do
    @char_counter_payload['second_string'] = 'sdfsafsa sadfsadfsadfsaf asdfsadfasdfasdf asdfasdfasdfasdf asdfasfsadfsdf asdfasdfsdfsadf asdfsfsdfsf sadfsadfsadfsafsdaf sadfasfsafasdfsd '
    post '/char-counters', params: @char_counter_payload
    response_data = JSON.parse(response.body)
    expect(response.status).to eq(422)
    expect(response_data['id']).to be_nil
  end

  it 'returns 422 if third_string has more than 104 chars' do
    @char_counter_payload['third_string'] = 'sdfsafsa sadfsadfsadfsaf asdfsadfasdfasdf asdfasdfasdfasdf asdfasfsadfsdf asdfasdfsdfsadf asdfsfsdfsf sadfsadfsadfsafsdaf sadfasfsafasdfsd '
    post '/char-counters', params: @char_counter_payload
    response_data = JSON.parse(response.body)
    expect(response.status).to eq(422)
    expect(response_data['id']).to be_nil
  end

  it 'returns 422 if first_string has no English chars' do
    @char_counter_payload['first_string'] = 'çç adfsdf ééá     dsfasdf'
    post '/char-counters', params: @char_counter_payload
    response_data = JSON.parse(response.body)
    expect(response.status).to eq(422)
    expect(response_data['id']).to be_nil
  end

  it 'returns 422 if second_string has no English chars' do
    @char_counter_payload['second_string'] = 'çç adfsdf ééá     dsfasdf'
    post '/char-counters', params: @char_counter_payload
    response_data = JSON.parse(response.body)
    expect(response.status).to eq(422)
    expect(response_data['id']).to be_nil
  end

  it 'returns 422 if third_string has no English chars' do
    @char_counter_payload['third_string'] = 'çç adfsdf ééá     dsfasdf'
    post '/char-counters', params: @char_counter_payload
    response_data = JSON.parse(response.body)
    expect(response.status).to eq(422)
    expect(response_data['id']).to be_nil
  end

  it 'returns 422 if first_string has numeric chars' do
    @char_counter_payload['first_string'] = '11 123adfsdf 123213   123dsfasdf'
    post '/char-counters', params: @char_counter_payload
    response_data = JSON.parse(response.body)
    expect(response.status).to eq(422)
    expect(response_data['id']).to be_nil
  end

  it 'returns 422 if second_string has numeric chars' do
    @char_counter_payload['second_string'] = '11 123adfsdf 123213   123dsfasdf'
    post '/char-counters', params: @char_counter_payload
    response_data = JSON.parse(response.body)
    expect(response.status).to eq(422)
    expect(response_data['id']).to be_nil
  end

  it 'returns 422 if third_string has numeric chars' do
    @char_counter_payload['third_string'] = '11 123adfsdf 123213   123dsfasdf'
    post '/char-counters', params: @char_counter_payload
    response_data = JSON.parse(response.body)
    expect(response.status).to eq(422)
    expect(response_data['id']).to be_nil
  end

  it 'must return Http Status Code 201 if first_last_word_counter is bigger than 0 after create' do
    post '/char-counters', params: @char_counter_payload
    response_data = JSON.parse(response.body)
    expect(response.status).to eq(201)
    expect(response_data['id']).to_not be_nil
    expect(response_data['first_last_word_counter']).to be >= 1
  end

  it 'must return Http Status Code 201 if second_last_word_counter is bigger than 0 after create' do
    post '/char-counters', params: @char_counter_payload
    response_data = JSON.parse(response.body)
    expect(response.status).to eq(201)
    expect(response_data['id']).to_not be_nil
    expect(response_data['second_last_word_counter']).to be >= 1
  end

  it 'must return Http Status Code 201 if third_last_word_counter is bigger than 0 after create' do
    post '/char-counters', params: @char_counter_payload
    response_data = JSON.parse(response.body)
    expect(response.status).to eq(201)
    expect(response_data['id']).to_not be_nil
    expect(response_data['third_last_word_counter']).to be >= 1
  end

  it 'must present an id after create' do
    post '/char-counters', params: @char_counter_payload
    response_data = JSON.parse(response.body)
    expect(response.status).to eq(201)
    expect(response_data['id']).to_not be_nil
  end

  it 'must not present an uuid after create' do
    post '/char-counters', params: @char_counter_payload
    response_data = JSON.parse(response.body)
    expect(response.status).to eq(201)
    expect(response_data['id']).to_not be_nil
    expect(response_data['uuid']).to be_nil
  end

  it 'must return Http Status Code 200 when found by id' do
    post '/char-counters', params: @char_counter_payload
    response_data = JSON.parse(response.body)

    get '/char-counters/:char_counter_id', char_counter_id: response_data['id']
    response_data = JSON.parse(response.body)

    expect(response.status).to eq(200)
    expect(response_data['id']).to_not be_nil
  end

  it 'must return Http Status Code 404 when not found by id' do
    post '/char-counters', params: @char_counter_payload

    get "/char-counters/asdfsdfsf", params: @char_counter_payload
    response_data = JSON.parse(response.body)

    expect(response.status).to eq(404)
    expect(response_data['id']).to be_nil
  end
end
