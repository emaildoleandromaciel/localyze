class CreateCharCounters < ActiveRecord::Migration[6.1]
  def change
    create_table :char_counters do |t|
      t.string :first_string
      t.string :second_string
      t.string :third_string
      t.integer :first_last_word_counter
      t.integer :second_last_word_counter
      t.integer :third_last_word_counter
      t.string :uuid

      t.timestamps
    end
  end
end
